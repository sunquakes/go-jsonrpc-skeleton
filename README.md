# Requirements

- Go >= 1.15

# Installation using Composer

- cd go-jsonrpc-skeleton
- go get -d ./...
- go run main.go

# Test
- Installed and started associated projects
- go run main.go
- Expected results
```
<nil>
7
2020/12/20 09:50:15 Listening http://127.0.0.1:3233
```

# Associated projects

[hyperf-skeleton](https://gitee.com/sunquakes/hyperf-skeleton)