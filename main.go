package main

import (
	"fmt"
	go_jsonrpc "github.com/sunquakes/go-jsonrpc"
	"go-jsonrpc-skeleton/jsonrpc"
)

func main() {
	result := new(jsonrpc.Result)
	c, _ := go_jsonrpc.NewClient("http", "127.0.0.1", "3232")
	err := c.Call("php/add", jsonrpc.Params{1, 6}, result, false)
	// data sent: {"id":"1604283212","jsonrpc":"2.0","method":"php/add","params":{"a":1,"b":6}}
	// data received: {"id":"1604283212","jsonrpc":"2.0","result":7}
	fmt.Println(err) // nil
	fmt.Println(*result) // 7

	s, _ := go_jsonrpc.NewServer("http", "127.0.0.1", "3233")
	s.Register(new(jsonrpc.Go))
	s.Start()
}
